//
//  SchoolDetailViewController.swift
//  NYCSchoolDemo
//
//  Created by TAPAN BISWAS on 7/13/19.
//  Copyright © 2019 TAPAN BISWAS. All rights reserved.
//

import UIKit

class SchoolDetailViewController: UIViewController {

    var school: School!
    var model: SchoolModel!
    
    @IBOutlet weak var schoolNameLabel: UILabel!
    @IBOutlet weak var schoolAddressLabel: UILabel!
    @IBOutlet weak var schoolPhoneLabel: UILabel!
    @IBOutlet weak var schoolFaxLabel: UILabel!
    @IBOutlet weak var schoolEMailLabel: UILabel!
    @IBOutlet weak var schoolWebSiteLabel: UILabel!
    @IBOutlet weak var schoolBoroughLabel: UILabel!
    @IBOutlet weak var satReadingScoreLabel: UILabel!
    @IBOutlet weak var satWritingScoreLabel: UILabel!
    @IBOutlet weak var satMathScoreLabel: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        let address = school.address ?? ""
        let city = school.city ?? ""
        let state = school.state ?? ""
        let zip = school.zip ?? ""
        
        schoolNameLabel.text = school.name ?? ""
        schoolAddressLabel.text = address + "\n" + city + "\n" + state + ", " + zip
        schoolBoroughLabel.text = school.borough ?? ""
        schoolPhoneLabel.text = school.phone ?? ""
        schoolFaxLabel.text = school.fax ?? ""
        schoolEMailLabel.text = school.email ?? ""
        schoolWebSiteLabel.text = school.website ?? ""
        
        let satScore = model.satscores.filter { (score) -> Bool in
            return score.dbn == school.dbn
        }
 
        if satScore.count >= 1 {
            let readingScore = satScore[0].readingAverage ?? 0
            let writingScore = satScore[0].writingAverage ?? 0
            let mathScore = satScore[0].mathAverage ?? 0
            
            satReadingScoreLabel.text = (readingScore > 0 ? "\(readingScore)" : "--")
            satWritingScoreLabel.text = (writingScore > 0 ? "\(writingScore)" : "--")
            satMathScoreLabel.text = (mathScore > 0 ? "\(mathScore)" : "--")
        } else {
            satReadingScoreLabel.text =  "--"
            satWritingScoreLabel.text =  "--"
            satMathScoreLabel.text = "--"
        }
    }

}
