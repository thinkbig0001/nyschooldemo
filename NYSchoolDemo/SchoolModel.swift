//
//  SchoolModel.swift
//  NYCSchoolDemo
//
//  Created by TAPAN BISWAS on 7/12/19.
//  Copyright © 2019 TAPAN BISWAS. All rights reserved.
//

import Foundation
import UIKit

struct School : Codable {
    var dbn: String
    var name: String?
    var overview: String?
    var borough: String?
    var address: String?
    var city: String?
    var state: String?
    var zip: String?
    var phone: String?
    var fax: String?
    var email: String?
    var website: String?
    var numStudents: Int?
    
    enum CodingKeys: String, CodingKey {
        case dbn
        case name = "school_name"
        case overview = "overview_paragraph"
        case borough
        case address = "primary_address_line_1"
        case city
        case state = "state_code"
        case zip
        case phone = "phone_number"
        case fax = "fax_number"
        case email = "school_email"
        case website
        case numStudents = "total_students"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        dbn = try values.decode(String.self, forKey: CodingKeys.dbn)
        name = try values.decodeIfPresent(String.self, forKey: CodingKeys.name)
        overview = try values.decodeIfPresent(String.self, forKey: CodingKeys.overview)
        borough = try values.decodeIfPresent(String.self, forKey: CodingKeys.borough)
        address = try values.decodeIfPresent(String.self, forKey: CodingKeys.address)
        city = try values.decodeIfPresent(String.self, forKey: CodingKeys.city)
        state = try values.decodeIfPresent(String.self, forKey: CodingKeys.state)
        zip = try values.decodeIfPresent(String.self, forKey: CodingKeys.zip)
        phone = try values.decodeIfPresent(String.self, forKey: CodingKeys.phone)
        fax = try values.decodeIfPresent(String.self, forKey: CodingKeys.fax)
        email = try values.decodeIfPresent(String.self, forKey: CodingKeys.email)
        website = try values.decodeIfPresent(String.self, forKey: CodingKeys.website)
        numStudents = Int(try values.decodeIfPresent(String.self, forKey: CodingKeys.numStudents) ?? "0")
     }

}

struct SATScore : Codable {
    var dbn: String
    var name: String?
    var numTestTakers: Int?
    var readingAverage: Int?
    var mathAverage: Int?
    var writingAverage: Int?
    
    enum CodingKeys: String, CodingKey {
        case dbn
        case name = "school_name"
        case numTestTakers = "num_of_sat_test_takers"
        case readingAverage = "sat_critical_reading_avg_score"
        case mathAverage = "sat_math_avg_score"
        case writingAverage = "sat_writing_avg_score"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        dbn = try values.decode(String.self, forKey: CodingKeys.dbn)
        name = try values.decodeIfPresent(String.self, forKey: CodingKeys.name)
        numTestTakers = Int(try values.decodeIfPresent(String.self, forKey: CodingKeys.numTestTakers) ?? "0")
        readingAverage = Int(try values.decodeIfPresent(String.self, forKey: CodingKeys.readingAverage) ?? "0")
        mathAverage = Int(try values.decodeIfPresent(String.self, forKey: CodingKeys.mathAverage) ?? "0")
        writingAverage = Int(try values.decodeIfPresent(String.self, forKey: CodingKeys.writingAverage) ?? "0")
    }
}

final class SchoolModel {
    var schools = [School]()
    var satscores = [SATScore]()

    func loadSchoolsFromRemoteURL(handler: @escaping ((HTTPURLResponse)->Void)) {
        let schoolURL = "https://data.cityofnewyork.us/resource/s3k6-pzi2.json"
        
        requestData(endpoint: schoolURL) { (data, response, error) in
            guard error == nil else {
                ShowAlert(title: "Error", message: "Unable to retrive remote data for school list \n\(error?.localizedDescription ?? "")")
                return
            }
            if let resp = response as? HTTPURLResponse {
                if resp.statusCode  != 200 {
                    ShowAlert(title: "Error", message: "Response Code: \(resp.statusCode) :: \(resp.debugDescription)")
                    return
                }
            }
            guard let returnedData = data else {
                ShowAlert(title: "Sorry", message: "No data received")
                return
            }
            do {
                let decoder = JSONDecoder()
                self.schools = try decoder.decode([School].self, from: returnedData)
                self.schools.sort(by: { (a, b) -> Bool in
                    return a.name! < b.name!
                })
                handler(response as! HTTPURLResponse)
            } catch  {
                ShowAlert(title: "Error", message: "Error trying to convert data to readable format")
                return
            }
            
        }
    }
    
    func loadSatScoresFromRemoteURL(handler: @escaping ((HTTPURLResponse)->Void)) {
        let satScoreURL = "https://data.cityofnewyork.us/resource/f9bf-2cp4.json"
        
        requestData(endpoint: satScoreURL) { (data, response, error) in
            guard error == nil else {
                ShowAlert(title: "Error", message: "Unable to retrive remote data for school list \n\(error?.localizedDescription ?? "")")
                return
            }
            if let resp = response as? HTTPURLResponse {
                if resp.statusCode  != 200 {
                    ShowAlert(title: "Error", message: "Response Code: \(resp.statusCode) :: \(resp.debugDescription)")
                    return
                }
            }
            guard let returnedData = data else {
                ShowAlert(title: "Sorry", message: "No data received")
                return
            }
            do {
                let decoder = JSONDecoder()
                self.satscores = try decoder.decode([SATScore].self, from: returnedData)
                handler(response as! HTTPURLResponse)
            } catch  {
                ShowAlert(title: "Error", message: "Error trying to convert data to readable format")
                return
            }
        }
    }
    
    private func requestData(endpoint: String, completionHandler: @escaping (Data?, URLResponse?, Error?) -> Void) {
        
        guard let url = URL(string: endpoint) else {
            ShowAlert(title: "Error", message: "Error trying to convert URL endpoint")
            return
        }
        let urlRequest = URLRequest(url: url)
        let session = URLSession.shared
        session.configuration.timeoutIntervalForRequest = 10
        session.configuration.waitsForConnectivity = false
        
        let task = session.dataTask(with: urlRequest) { (data, response, error) in
            completionHandler(data, response, error)
        }
        task.resume()
    }

}

public func ShowAlert(title: String, message: String) {
    OperationQueue.main.addOperation {
        
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        let okAction = UIAlertAction(title: "OK", style: .destructive, handler: nil)
        
        alertController.addAction(okAction)
        
        if var topController = UIApplication.shared.keyWindow?.rootViewController {
            while let presentedViewController = topController.presentedViewController {
                topController = presentedViewController
            }
            topController.present(alertController, animated: true)
        }
    }
}

