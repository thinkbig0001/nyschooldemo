//
//  SchoolListViewController.swift
//  NYCSchoolDemo
//
//  Created by TAPAN BISWAS on 7/12/19.
//  Copyright © 2019 TAPAN BISWAS. All rights reserved.
//

import UIKit

class SchoolListViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var tableView: UITableView!
    
    var model = SchoolModel()                   //Contains runtime dataset that changes when filtering
    var unfilteredModel = SchoolModel()         //Contains the unflitered data set received from remote URL

    var selectedIndexPath: IndexPath? = nil     //Keeps track selected row in the table.
    
    let reuseIdentifier = "schoolCell"
    
    override func viewDidLoad() {
        super.viewDidLoad()

        tableView.register(UITableViewCell.self, forCellReuseIdentifier: reuseIdentifier)
        
        //For Pulldown Refresh - add refresh control
        tableView.refreshControl = UIRefreshControl()
        tableView.refreshControl?.tintColor = .blue
        
        //Add action for pulldown refresh action
        tableView.refreshControl?.addTarget(self, action: #selector(loadData), for: .valueChanged)
        
        //Set up searchBar delegate
        searchBar.delegate = self

        //Load data from Remote
        loadData()
    }

    @objc private func loadData() {
        unfilteredModel.schools.removeAll()
        model.schools.removeAll()
        
        //Load School List Data into Model
        tableView.refreshControl?.beginRefreshing()
        unfilteredModel.loadSchoolsFromRemoteURL() { (response)  in
            self.unfilteredModel.schools.forEach({ (school) in
                self.model.schools.append(school)
            })
            OperationQueue.main.addOperation {
                self.tableView.refreshControl?.endRefreshing()
                
                self.tableView.reloadData()
            }
        }
        
        //Load SAT Scores data into model
        tableView.refreshControl?.beginRefreshing()
        model.loadSatScoresFromRemoteURL() { (response)  in
            OperationQueue.main.addOperation {
                self.tableView.refreshControl?.endRefreshing()
            }
        }
        tableView.refreshControl?.endRefreshing()
    }
    
    
    // MARK: - Table view data source

    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return model.schools.count
    }

    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: reuseIdentifier, for: indexPath)

        guard model.schools.count > 0 else { return cell }
        
        cell.textLabel?.text = model.schools[indexPath.row].name
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedIndexPath = indexPath
        performSegue(withIdentifier: "showDetail", sender: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showDetail"  && selectedIndexPath != nil {
            let destVC = segue.destination as! SchoolDetailViewController
            
            destVC.model = model
            destVC.school = model.schools[selectedIndexPath!.row]
        }
    }
}

extension SchoolListViewController: UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        model.schools = unfilteredModel.schools.filter({ (school) -> Bool in
            if searchText.isEmpty || searchText.replacingOccurrences(of: " ", with: "") == "" {
                return true
            } else {
                return school.name?.contains(searchText) ?? false
            }
        })
        tableView.reloadData()
    }
}
