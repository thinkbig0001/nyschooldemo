//
//  NYSchoolDemoTests.swift
//  NYSchoolDemoTests
//
//  Created by TAPAN BISWAS on 7/13/19.
//  Copyright © 2019 TAPAN BISWAS. All rights reserved.
//

import XCTest
@testable import NYSchoolDemo

class NYSchoolDemoTests: XCTestCase {

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testJSONDecoderForSuccess() {
        //Test1: To test decoding will succeed if required key is present for non-optional value
        let testData1 = Data("""
        {
        "dbn":"02M260",
        "school_name":"Clinton School Writers & Artists, M.S. 260",
        "total_students":"376",
        "primary_address_line_1":"10 East 15th Street",
        "city":"Manhattan",
        "zip":"10003",
        "state_code":"NY",
        "borough":"MANHATTAN"
        }
        """.utf8)
 
        XCTAssertNoThrow(try JSONDecoder().decode(School.self, from: testData1))
    }
    
    func testJSONDecoderForFailure() {
        //Test2: To test decoding will fail if required key is not there for non-optional value
        //We should have "dbn" as one of the required key-value pair in the JSON
        let testData2 = Data("""
        {
        "school_name":"Clinton School Writers & Artists, M.S. 260",
        "total_students":"376",
        "primary_address_line_1":"10 East 15th Street",
        "city":"Manhattan",
        "zip":"10003",
        "state_code":"NY",
        "borough":"MANHATTAN"
        }
        """.utf8)
        
        XCTAssertThrowsError(try JSONDecoder().decode(School.self, from: testData2)) { error in
            if case .keyNotFound(let key, _)? = error as? DecodingError {
                XCTAssertNotEqual("type", key.stringValue)
            } else {
                XCTFail("Expected '.keyNotFound' but got \(error)")
            }
        }
        
    }

    func testRemoteDataLoadSuccess() {
        let model = SchoolModel()
        
        model.loadSchoolsFromRemoteURL { (resp) in
            XCTAssert(model.schools.count > 0, "Remote data load success test failed")
        }
    }
    
    func testViewControllers() {
        let vc1 = SchoolListViewController()
        XCTAssert(vc1.isMember(of: SchoolListViewController.self))
        
        let vc2 = SchoolDetailViewController()
        XCTAssert(vc2.isMember(of: SchoolDetailViewController.self))

        vc1.model.loadSchoolsFromRemoteURL { (resp) in
            XCTAssert(vc1.model.schools.count > 0)

            vc2.model = vc1.model
            XCTAssert(vc2.model.schools.count > 0)
       }

    }
    
    func testPerformanceOfDataLoad() {
        // This is an example of a performance test case.
        self.measure {
            let model = SchoolModel()
            
            model.loadSchoolsFromRemoteURL { (resp) in
            }
            model.loadSatScoresFromRemoteURL(handler: { (resp) in
            })
       }
    }

}
