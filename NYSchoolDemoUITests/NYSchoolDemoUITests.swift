//
//  NYSchoolDemoUITests.swift
//  NYSchoolDemoUITests
//
//  Created by TAPAN BISWAS on 7/13/19.
//  Copyright © 2019 TAPAN BISWAS. All rights reserved.
//

import XCTest

class NYSchoolDemoUITests: XCTestCase {

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.

        // In UI tests it is usually best to stop immediately when a failure occurs.
        continueAfterFailure = false

        // UI tests must launch the application that they test. Doing this in setup will make sure it happens for each test method.
        XCUIApplication().launch()

        // In UI tests it’s important to set the initial state - such as interface orientation - required for your tests before they run. The setUp method is a good place to do this.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testDataLoadInTableView() {
        
        let app = XCUIApplication()
        
        //wait for the data load
        let element = app.tables.cells.element(boundBy: 0)
        let predicate = NSPredicate(format: "exists = true")
        let expectation = XCTNSPredicateExpectation(predicate: predicate, object: element)
        
        let result = XCTWaiter().wait(for: [expectation], timeout: 5)
        XCTAssertEqual(.completed, result)
        
        //Check there are 1 or more rows loaded
        XCTAssertTrue(app.tables.cells.count > 0)
        
    }

    func testNavigationAndContent() {
        
        let app = XCUIApplication()
        
        let model = SchoolModel()
        
        //Load Model data independent of the tableView loading it
        model.loadSchoolsFromRemoteURL { (resp) in
        }
        model.loadSatScoresFromRemoteURL { (resp) in
        }
        
        //wait for the data load
        testDataLoadInTableView()
        
        let tableView = app.tables  //this works because there is only one table
        
        //Tap row 0, 3, 7, 10
        var names: [String] = []
        
        names.append(tableView.staticTexts.element(boundBy: 0).label)
        names.append(tableView.staticTexts.element(boundBy: 3).label)
        names.append(tableView.staticTexts.element(boundBy: 7).label)
        names.append(tableView.staticTexts.element(boundBy: 10).label)
        
        names.forEach { (name) in
            let cell = tableView.staticTexts[name]
            cell.tap()
            //Check for transition to SchoolDetailView
            XCTAssert(app.navigationBars.element(boundBy: 0).identifier == "NYSchoolDemo.SchoolDetailView")
            
            //Check the name on Detail View
            XCTAssert(app.staticTexts[name].exists)
            
            //Check SAT Score Matches with SAT Data for this school
            let school = model.schools.first(where: { (school) -> Bool in
                return school.name == name
            })
            let satScore = model.satscores.first(where: { (satscore) -> Bool in
                return satscore.dbn == school!.dbn
            })
            
            print("dbn : \(satScore?.dbn ?? ""), name: \(satScore?.name ?? ""), numTestTakers: \(satScore?.numTestTakers ?? 0)")
            
            print("readingAverage: \(satScore?.readingAverage ?? 0)", "wrtingAverage: \(satScore?.writingAverage ?? 0)", "mathAverage: \(satScore?.mathAverage ?? 0)")
            
            let readingScore = (satScore?.mathAverage == nil ? "--" : "\((satScore?.readingAverage)!)")
            let writingScore = (satScore?.mathAverage == nil ? "--" : "\((satScore?.writingAverage)!)")
            let mathScore = (satScore?.mathAverage == nil ? "--" : "\((satScore?.mathAverage)!)")
            XCTAssert(app.staticTexts[readingScore].exists)
            XCTAssert(app.staticTexts[writingScore].exists)
            XCTAssert(app.staticTexts[mathScore].exists)

            //Tap back on Navigation bar button "<NYC Schools"
            app.navigationBars["NYSchoolDemo.SchoolDetailView"].buttons["NYC Schools"].tap()
            
            //Check for transition back to SchoolListView
            XCTAssert(app.navigationBars.element(boundBy: 0).identifier == "NYC Schools")
        }
    }

}

